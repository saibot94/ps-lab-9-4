var request = new XMLHttpRequest();
var method = "GET";
var url = "https://www.reddit.com/r/programming.json";

function showData(children) {
    var list = document.getElementById("my_reddit_list");

    for (var i = 0; i < children.length; i++) {
        var article = children[i].data;
        var newElem = document.createElement("li");
        var link = document.createElement("a");
        link.href = article.url;

        var text = document
            .createTextNode(article.ups + " - " + article.title);
        link.appendChild(text);
        newElem.appendChild(link);
        list.appendChild(newElem);
    }
}

var eventHandlingFunction = function () {
    if (request.readyState == XMLHttpRequest.DONE &&
        request.status == 200) {
        var response = JSON.parse(request.responseText);
        showData(response.data.children);
    }
}

request.open(method, url, true);
request.onreadystatechange = eventHandlingFunction;
request.send();






